package main

import (
	"archive/tar"
	"compress/gzip"
	"crypto/rand"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestTar(t *testing.T) {
	assert := assert.New(t)

	tempDir, err := ioutil.TempDir("", "explode-test-TestTar")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(tempDir)

	// create tar file containing a simple text file.
	tf, err := os.Create(filepath.Join(tempDir, "foo.tar"))
	tw := tar.NewWriter(tf)
	data := []byte("baz baz baz baz")
	if err = tw.WriteHeader(&tar.Header{
		Name:    "bar.txt",
		Size:    int64(len(data)),
		Mode:    0644,
		ModTime: time.Now(),
	}); err != nil {
		t.Fatal(err)
	}
	if _, err := tw.Write(data); err != nil {
		t.Fatal(err)
	}
	if err := tw.Close(); err != nil {
		t.Fatal(err)
	}
	if err := tf.Close(); err != nil {
		t.Fatal(err)
	}

	// explode it
	if err := handleAll(tempDir); err != nil {
		t.Fatal(err)
	}

	// extracted file should exist
	data, err = ioutil.ReadFile(filepath.Join(tempDir, "bar.txt"))
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal("baz baz baz baz", string(data))

	// input file should be gone
	_, err = os.Stat(tf.Name())
	if !os.IsNotExist(err) {
		t.Fatal("input file was not deleted")
	}
}

func TestTarGz(t *testing.T) {
	assert := assert.New(t)

	tempDir, err := ioutil.TempDir("", "explode-test-TestTarGz")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(tempDir)

	// create tar file containing a simple text file.
	tf, err := os.Create(filepath.Join(tempDir, "foo.tar.gz"))

	gf := gzip.NewWriter(tf)

	tw := tar.NewWriter(gf)
	data := []byte("baz baz baz baz")
	if err = tw.WriteHeader(&tar.Header{
		Name:    "bar.txt",
		Size:    int64(len(data)),
		Mode:    0644,
		ModTime: time.Now(),
	}); err != nil {
		t.Fatal(err)
	}
	if _, err := tw.Write(data); err != nil {
		t.Fatal(err)
	}
	if err := tw.Close(); err != nil {
		t.Fatal(err)
	}
	if err := gf.Flush(); err != nil {
		t.Fatal(err)
	}
	if err := tf.Close(); err != nil {
		t.Fatal(err)
	}

	// explode it
	if err := handleAll(tempDir); err != nil {
		t.Fatal(err)
	}

	// extracted file should exist
	data, err = ioutil.ReadFile(filepath.Join(tempDir, "bar.txt"))
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal("baz baz baz baz", string(data))

	// input file should be gone
	_, err = os.Stat(tf.Name())
	if !os.IsNotExist(err) {
		t.Fatal("input file was not deleted")
	}
}

func TestRarMultiPart(t *testing.T) {
	assert := assert.New(t)

	tempDir, err := ioutil.TempDir("", "explode-test-TestRar")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(tempDir)

	// create tar file containing a simple text file.
	inputData := randomData(8192)
	if err := ioutil.WriteFile(filepath.Join(tempDir, "bar.txt"), inputData, 0644); err != nil {
		t.Fatal(err)
	}

	cmd := exec.Command("sh", "-c", "rar a -ma4 -vn -v1 foo.rar bar.txt")
	cmd.Dir = tempDir

	output, err := cmd.Output()
	t.Logf("%s\n", output)
	if err != nil {
		t.Fatal(err)
	}
	if err := os.Remove(filepath.Join(tempDir, "bar.txt")); err != nil {
		t.Fatal(err)
	}

	// list the input rar file parts
	var inputFiles []string
	if err := filepath.Walk(tempDir, func(path string, info os.FileInfo, err error) error {
		if tempDir != path {
			inputFiles = append(inputFiles, path)
		}
		return nil
	}); err != nil {
		t.Fatal(err)
	}

	// explode it
	if err := handleAll(tempDir); err != nil {
		t.Fatal(err)
	}

	// extracted file should exist
	data, err := ioutil.ReadFile(filepath.Join(tempDir, "bar.txt"))
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(data, inputData)

	// input files should be gone
	for _, in := range inputFiles {
		_, err = os.Stat(in)
		if !os.IsNotExist(err) {
			t.Fatalf("input file was not deleted : %s", in)
		}
		t.Logf("input file was  deleted as expected: %s", in)
	}
}

func randomData(i int) []byte {
	buf := make([]byte, i)
	if _, err := rand.Read(buf); err != nil {
		panic(err)
	}
	return buf
}
