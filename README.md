explode
=======

Explode traverses a directory tree, expanding any archive files it finds and removing the archive afterwards.

Supported currently are :

`tar`
`tar.gz`
`rar` (including multipart)

