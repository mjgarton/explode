package main

import (
	"archive/tar"
	"compress/gzip"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"github.com/nwaples/rardecode"
)

func handleAll(path string) error {

	var err error
	path, err = filepath.Abs(path)
	if err != nil {
		return err
	}

	var f filepath.WalkFunc = func(path string, info os.FileInfo, err error) error {
		switch {
		case strings.HasPrefix(path, "."):
			return nil
		case strings.HasSuffix(path, ".tar.gz"):
			return handleTarGz(path)
		case strings.HasSuffix(path, ".tar"):
			return handleTar(path)
		case strings.HasSuffix(path, ".rar"):
			return handleRar(path)
		}
		return nil
	}

	return filepath.Walk(path, f)
}

func handleTarGz(path string) error {
	destination, _ := filepath.Split(path)

	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()

	gr, err := gzip.NewReader(f)
	if err != nil {
		return err
	}

	tr := tar.NewReader(gr)
	err = doHandleTar(tr, destination)
	if err != nil {
		return err
	}

	return os.Remove(path)
}

func handleTar(path string) error {
	destination, _ := filepath.Split(path)

	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()

	tr := tar.NewReader(f)
	err = doHandleTar(tr, destination)
	if err != nil {
		return err
	}

	return os.Remove(path)
}

func doHandleTar(tr *tar.Reader, destination string) error {
	for {
		header, err := tr.Next()
		if err != nil {
			if err == io.EOF {
				return nil
			}
			return err
		}

		if err := checkPath(header.Name, destination); err != nil {
			return err
		}

		switch header.Typeflag {
		case tar.TypeReg, tar.TypeRegA:
			fname := filepath.Join(destination, header.Name)
			err := os.MkdirAll(filepath.Dir(fname), 0755)
			if err != nil {
				return err
			}

			out, err := os.Create(fname)
			if err != nil {
				return err
			}
			defer out.Close()

			if err := out.Chmod(header.FileInfo().Mode()); err != nil {
				return err
			}

			if _, err := io.Copy(out, tr); err != nil {
				return err
			}

			if err := out.Close(); err != nil {
				return err
			}

			fi := header.FileInfo()
			if err := os.Chtimes(fname, fi.ModTime(), fi.ModTime()); err != nil {
				return err
			}

		case tar.TypeLink:
			return errors.New("can't untar hard link files yet")
		case tar.TypeSymlink:
			return errors.New("can't untar symbolic link files yet")
		case tar.TypeChar, tar.TypeBlock:
			return errors.New("can't untar char device files yet")
		case tar.TypeDir:
			dirName := filepath.Join(destination, header.Name)
			if err := os.MkdirAll(dirName, 0755); err != nil {
				return err
			}
		case tar.TypeFifo:
			return errors.New("can't untar fifo files yet")
		case tar.TypeCont:
			return errors.New("can't untar 'reserved type' files yet")
		case tar.TypeGNUSparse:
			return errors.New("can't untar sparse files yet")
		default:
			panic(fmt.Sprintf("bug: unhandled tar header type : %v", header.Typeflag))
		}
	}
}

func handleRar(path string) error {
	destination, _ := filepath.Split(path)

	rr, err := rardecode.OpenReader(path, "")
	if err != nil {
		return err
	}
	defer rr.Close()

	for {
		header, err := rr.Next()
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}

		if err := checkPath(header.Name, destination); err != nil {
			return err
		}

		fname := filepath.Join(destination, header.Name)

		if header.IsDir {
			if err := os.MkdirAll(fname, 0755); err != nil {
				return err
			}
		} else {
			if err := os.MkdirAll(filepath.Dir(fname), 0755); err != nil {
				return err
			}

			err := os.MkdirAll(filepath.Dir(fname), 0755)
			if err != nil {
				return err
			}

			out, err := os.OpenFile(fname, os.O_RDWR|os.O_CREATE|os.O_TRUNC, header.Mode())
			defer out.Close()

			_, err = io.Copy(out, rr)
			if err != nil {
				return err
			}
		}
	}

	for _, pf := range rr.Volumes() {
		if err := os.Remove(pf); err != nil {
			return err
		}
	}

	return nil
}

func checkPath(filePath string, destination string) error {
	destpath := filepath.Join(destination, filePath)
	if !strings.HasPrefix(destpath, filepath.Clean(destination)) {
		return fmt.Errorf("%s: illegal file path", filePath)
	}
	return nil
}
