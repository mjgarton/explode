package main

import (
	"log"
)

func main() {
	if err := handleAll("."); err != nil {
		log.Fatal(err)
	}
}
